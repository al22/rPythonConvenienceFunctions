

<!-- Readme.md is generated from Readme.org. Please edit that file -->

# rPythonConvenienceFunctions R package

This package makes interaction with `python` more convenient from
within `R`.

The technical side of interacting with `python` is covered by the
[rPython](https://cran.r-project.org/web/packages/rPython/index.html) package, which makes it possible to exchange data between `R` and
`python` and to call `python` code from within `R`.  The provided
functionality is pretty basic, though.

Especially, exchanging data between `R` and `python` is more tricky
when it comes to more complex data than simple numbers or vectors,
as it becomes necessary to work around some quirks on the way
`python` handles and serializes data.

Here, `rPythonConvenienceFunctions` can help with some high-level
functions, e.g. direct transfer between `R` `data.frame` and `python`
`pandas:DataFrame` in both ways.

## Example Usage

This is the example from the help page of `python_send_dfasDF` (see
with ).

We send some data to `python`, verify that is in the expected form and
transfer it back to `R`.

```R
library("rPythonConvenienceFunctions")

## load data
data(iris)

## transfer to python
python_send_dfasDF("iri", iris)

## check
rPython::python.exec('print type(iri)')
rPython::python.method.call('iri', 'info')
rPython::python.exec('print iri.head(3)')

## transfer back to R
iris_back <- python_get_DFasdf("iri")

## have a look
head(iris_back)
```

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Petal.Length</th>
<th scope="col" class="org-right">Sepal.Length</th>
<th scope="col" class="org-right">Petal.Width</th>
<th scope="col" class="org-right">Sepal.Width</th>
<th scope="col" class="org-left">Species</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">1.4</td>
<td class="org-right">5.1</td>
<td class="org-right">0.2</td>
<td class="org-right">3.5</td>
<td class="org-left">setosa</td>
</tr>


<tr>
<td class="org-right">1.4</td>
<td class="org-right">4.9</td>
<td class="org-right">0.2</td>
<td class="org-right">3</td>
<td class="org-left">setosa</td>
</tr>


<tr>
<td class="org-right">1.3</td>
<td class="org-right">4.7</td>
<td class="org-right">0.2</td>
<td class="org-right">3.2</td>
<td class="org-left">setosa</td>
</tr>


<tr>
<td class="org-right">1.5</td>
<td class="org-right">4.6</td>
<td class="org-right">0.2</td>
<td class="org-right">3.1</td>
<td class="org-left">setosa</td>
</tr>


<tr>
<td class="org-right">1.4</td>
<td class="org-right">5</td>
<td class="org-right">0.2</td>
<td class="org-right">3.6</td>
<td class="org-left">setosa</td>
</tr>


<tr>
<td class="org-right">1.7</td>
<td class="org-right">5.4</td>
<td class="org-right">0.4</td>
<td class="org-right">3.9</td>
<td class="org-left">setosa</td>
</tr>
</tbody>
</table>

## Installation

Installation directly from gitlab is possible, when the package \`devtools\`
is installed.

As R dependencies you'll just need the
[rPython](<https://cran.r-project.org/web/packages/rPython/index.html>)
package.  This in turn has obviously a system dependency on `python`.

```R
install.packages("rPython")
```

Once all dependencies are installed, get this package with

```R
library("devtools")
install_git("https://gitlab.com/al22/rPythonConvenienceFunctions.git")
```


<!--
Local Variables:
 mode: gfm
 markdown-command: "marked"
End:
-->
